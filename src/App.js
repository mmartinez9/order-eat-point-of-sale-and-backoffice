import React, { Component } from "react";
import Router from "./router";
import red from "@material-ui/core/colors/red";

import {
  createMuiTheme,
  responsiveFontSizes,
  ThemeProvider
} from "@material-ui/core/styles";

let theme = createMuiTheme({
  palette: {
    primary: red,
    secondary: { 
      main: '#000'
    }
  },
  status: {
    danger: "orange"
  }
});
theme = responsiveFontSizes(theme);

class App extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <div className="App">
          <Router />
        </div>
      </ThemeProvider>
    );
  }
}

export default App;
