import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Badge from "@material-ui/core/Badge";

const useStyles = makeStyles({
  badge: {
    width: "100%"
  }
});

function ProductCount(props) {
  const classes = useStyles();
  const { count, children } = props;
  return (
    <Badge className={classes.badge} badgeContent={count} color="primary">
      {children}
    </Badge>
  );
}

export default ProductCount;
