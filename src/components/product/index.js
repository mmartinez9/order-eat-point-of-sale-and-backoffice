import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import ProductCount from "./ProductCount";

const useStyles = makeStyles(theme => ({
  card: {
    width: "100%",
    height: "100%"
  },
  media: {
    height: 140
  },
  actions: {
    backgroundColor: theme.palette.paper,
    justifyContent: "space-between"
  }
}));

const ProductContainer = ({ product }) => {
  //   const { id, image, name, price } = product;

  const classes = useStyles();

  //   const count = selectedProducts.filter(p => p.id === id).length;
  const count = 3;

  const handleAdd = () => {
    // addProduct(product);
  };

  const handleRemove = () => {
    // removeProduct(id);
  };

  return (
    <ProductCount count={count}>
      <Card className={classes.card}>
        <CardActionArea onClick={handleAdd}>
          <CardMedia
            className={classes.media}
            image={
              product.main_photo_url
                ? product.main_photo_url
                : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR3cxTB73O54ES37Xy8lHqwcp5Rl3fXqgAEsVEcxPVeozBXgMMbxw&s"
            }
            title={"asdasd"}
          />
        </CardActionArea>
        <CardActions className={classes.actions}>
          <Button size="small" onClick={handleAdd}>
            <Typography variant="inherit" noWrap>
              {product.name}
            </Typography>
          </Button>
          {count > 0 && (
            <Button size="small" color="primary" onClick={handleRemove}>
              Quitar
            </Button>
          )}
        </CardActions>
      </Card>
    </ProductCount>
  );
};

export default ProductContainer;
