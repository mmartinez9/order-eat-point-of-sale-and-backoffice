import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import Product from "../product";
import { Grid } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper
  },
  gridList: {
    height: "100%",
    width: "100%",
    overflowX: "hidden",
    padding: theme.spacing(1),
    justifyContent: "center"
  },
  item: {
    marginBottom: 10
  },
  icon: {
    color: "rgba(255, 255, 255, 0.54)"
  }
}));

const ProductList = ({ products }) => {
  const classes = useStyles();

  //   const handleAddProduct = product => {
  //     const { selectedProducts, updateProducts } = props;

  //     const newSelectedProducts = selectedProducts.map(p => p);

  //     newSelectedProducts.push(product);

  //     updateProducts(newSelectedProducts);
  //   };

  //   const handleRemoveProduct = productId => {
  //     const { selectedProducts, updateProducts } = props;

  //     const newSelectedProducts = selectedProducts.map(p => p);

  //     let productIndex = null;

  //     for (let i = newSelectedProducts.length - 1; i >= 0; i--) {
  //       if (newSelectedProducts[i].id === productId) {
  //         productIndex = i;
  //         break;
  //       }
  //     }

  //     newSelectedProducts.splice(productIndex, 1);

  //     updateProducts(newSelectedProducts);
  //   };

  const productsUI = products.map(p => (
    <Grid key={p.id} item xs={6} sm={4} md={3} className={classes.item}>
      <Product
        product={p}
        // addProduct={handleAddProduct}
        // removeProduct={handleRemoveProduct}
      />
    </Grid>
  ));

  return (
    <div className={classes.root}>
      <GridList spacing={5} className={classes.gridList}>
        <GridListTile
          key="Subheader"
          cols={2}
          style={{ height: "auto" }}
        ></GridListTile>
        {productsUI}
      </GridList>
    </div>
  );
};

export default ProductList;
