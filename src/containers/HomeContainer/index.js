import React, { useEffect } from "react";
import { connect } from "react-redux";
import Welcome from "./welcome";
import LoggedLayout from "../../components/LoggedLayout";
import { getProducts, getCategories } from "../../redux/actions/products";
import Loader from "../../components/Loader";
import Product from "../../components/product";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/styles";
import ProductList from "../../components/productlist";

const useStyles = makeStyles({
  media: {
    height: 140
  }
});

const Home = ({
  products,
  categories,
  getProductsAction,
  getCategoriesAction,
  loading
}) => {
  const classes = useStyles();
  useEffect(() => {
    // getProductsAction();

    getCategoriesAction();
  }, []);

  console.log(products);
  return (
    <LoggedLayout>
      {loading ? (
        <Loader />
      ) : (
        products &&
        products.products.length > 0 && (
          <>
            <h2>{products.name}</h2>

            <ProductList products={products.products} />
          </>
        )
      )}
    </LoggedLayout>
  );
};

const mapStateToProps = state => ({
  products: state.products.products,
  categories: state.categories.categories,
  loading: state.products.loading
});

const mapDispatchToProps = dispatch => ({
  getProductsAction: categoryId => dispatch(getProducts(categoryId)),
  getCategoriesAction: () => dispatch(getCategories())
});
export default connect(mapStateToProps, mapDispatchToProps)(Home);
