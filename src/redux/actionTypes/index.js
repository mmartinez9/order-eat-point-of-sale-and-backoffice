import * as login from "./login";
import * as products from "./products";
import * as categories from "./categories";
import * as order from "./order";

const combined = { ...login, ...categories, ...products, ...order };

export default combined;
