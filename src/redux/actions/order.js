import actions from "../actionTypes";
import * as userService from "../../services/api/routes/userServices";

const setLoadingAction = value => ({
  type: actions.SET_LOADING,
  value
});

const setOrderAction = value => ({
  type: actions.SET_ORDER,
  value
});

const setOrderListAction = value => ({
  type: actions.SET_ORDER_LIST,
  value
});

export const getOrderByTable = (table) => dispatch => {
    dispatch(setLoadingAction(true));
    return userService.getOrderByTable(table).then(
      response => {
        dispatch(setLoadingAction(false));
        dispatch(setOrderListAction(response.data));
      },
      error => {
        dispatch(setLoadingAction(false));
        throw error;
      }
    );
  };

export const getAllPendingOrders = () => dispatch => {
  dispatch(setLoadingAction(true));
  return userService.getAllPendingOrders().then(
    response => {
      dispatch(setLoadingAction(false));
      dispatch(setOrderListAction(response.data));
    },
    error => {
      dispatch(setLoadingAction(false));
      throw error;
    }
  );
};

export const getOrder = id => dispatch => {
  dispatch(setLoadingAction(true));
  return userService.getOrder(id).then(
    response => {
      dispatch(setLoadingAction(false));
      dispatch(setOrderAction(responde.data));
    },
    error => {
      dispatch(setLoadingAction(false));
      throw error;
    }
  );
};

export const createOrder = order => dispatch => {
  dispatch(setLoadingAction(true));
  return userService.createOrder(order).then(
    response => {
      dispatch(setLoadingAction(false));
      // Show success message and redirect to home
    },
    error => {
      dispatch(setLoadingAction(false));
      throw error;
    }
  );
};

export const updateOrder = newOrder => dispatch => {
  dispatch(setLoadingAction(true));
  return userService.updateOrder(order).then(
    response => {
      dispatch(setLoadingAction(false));
      // Same as create: show success message
    },
    error => {
      dispatch(setLoadingAction(false));
      throw error;
    }
  );
};
