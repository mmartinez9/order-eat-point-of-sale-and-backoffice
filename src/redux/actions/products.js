import actions from "../actionTypes";
import * as userService from "../../services/api/routes/userServices";

const setLoadingAction = value => ({
  type: actions.SET_LOADING,
  value
});

const setProductsAction = value => ({
  type: actions.SET_PRODUCTS,
  value
});

const setCategoriesAction = value => ({
  type: actions.SET_CATEGORIES,
  value
});

export const getProducts = id => dispatch => {
  dispatch(setLoadingAction(true));
  return userService.getProducts(id).then(
    response => {
      dispatch(setLoadingAction(false));

      console.log("ACAAAA", response);
      dispatch(setProductsAction(response.data));
    },
    error => {
      dispatch(setLoadingAction(false));
      throw error;
    }
  );
};

export const getCategories = () => dispatch => {
  dispatch(setLoadingAction(true));
  return userService.getCategories().then(
    response => {
      dispatch(setLoadingAction(false));
      dispatch(setCategoriesAction(response.data));
    },
    error => {
      dispatch(setLoadingAction(false));
      throw error;
    }
  );
};
