import actions from "../actionTypes";

const initialState = {
  loading: false,
  categories: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actions.SET_LOADING:
      return {
        ...state,
        loading: action.value
      };
    case actions.SET_CATEGORIES:
      return {
        ...state,
        categories: action.value
      };
    default:
      return state;
  }
};
