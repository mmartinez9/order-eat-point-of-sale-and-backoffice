import { combineReducers } from "redux";
import login from "./login";
import categories from "./categories";
import products from "./products";
import order from './order'

export default combineReducers({
  login,
  categories,
  products,
  order
});
