import actions from "../actionTypes";

const initialState = {
  loading: false,
  order: null,
  orderList: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actions.SET_LOADING:
      return {
        ...state,
        loading: action.value
      };
    case actions.SET_ORDER:
      return {
        ...state,
        order: action.value
      };
    case actions.SET_ORDER_LIST:
      return {
        ...state,
        orderList: action.value
      };
    default:
      return state;
  }
};
