import actions from "../actionTypes";

const initialState = {
  loading: false,
  products: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actions.SET_LOADING:
      return {
        ...state,
        loading: action.value
      };
    case actions.SET_PRODUCTS:
      return {
        ...state,
        products: action.value
      };
    default:
      return state;
  }
};
