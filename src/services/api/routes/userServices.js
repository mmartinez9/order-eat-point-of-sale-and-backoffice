import { authAxiosCall } from "../axiosCall";

const institution = "5e0847b1938de3001296a995";
// PRODUCTS
export const getProducts = async id => {
  console.log(id);
  return authAxiosCall(
    `/api/v1/institutions/${institution}/product_categories/${id}`,
    {
      method: "GET"
    }
  );
};

// CATEGORIES
export const getCategories = async () =>
  authAxiosCall(`/api/v1/institutions/${institution}/product_categories`, {
    method: "GET"
  });

// ORDERS

export const getAllOrders = async () =>
  authAxiosCall(`/api/v1/institutions/${institution}/orders`, {
    method: "GET"
  });

export const getOrder = async id =>
  authAxiosCall(`/api/v1/institutions/${institution}/orders/${id}`, {
    method: "GET"
  });

export const getOrderByTable = async id =>
  authAxiosCall(`/api/v1/institutions/${institution}tables/${id}/orders/`, {
    method: "GET"
  });

export const getAllPendingOrders = async id =>
  authAxiosCall(`/api/v1/institutions/${institution}/pending-orders/`, {
    method: "GET"
  });

export const createOrder = async order =>
  authAxiosCall(`/api/v1/institutions/${institution}/orders`, {
    method: "POST",
    body: JSON.stringify({
      ...order
    })
  });

export const updateOrder = async order =>
  authAxiosCall(`/api/v1/institutions/${institution}/orders`, {
    method: "PUT",
    body: JSON.stringify({
      ...order
    })
  });
